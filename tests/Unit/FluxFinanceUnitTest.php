<?php

namespace Tests\Unit;

use App\FinanceReport;
use App\FluxFinance;
use App\RapportFinance;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use PHPUnit\Framework\TestCase;

class FluxFinanceUnitTest extends TestCase
{

    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();
    }



    /** @test */
    function un_rapport_de_finance_generer_le_chiffre_d_affaire_pour_une_periode()
    {

       //on a une date
       $carbon = Carbon::now();
       //on a une date de début
       $dbt = $carbon->subMonths(7);

       $fin = $dbt->addMonths(2);

      FluxFinance::getFinanceOverTime($dbt->toDateString(),$fin->toDateString());

    }
}
