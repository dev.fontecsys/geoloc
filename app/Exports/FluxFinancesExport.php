<?php

namespace App\Exports;

use App\FluxFinance;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;

class FluxFinancesExport implements WithTitle, FromQuery,WithEvents,
WithMapping,WithHeadings,WithColumnFormatting,ShouldAutoSize
{
    use Exportable;

    protected  $month,$year,$vehicule_id,$q,$type;

    public function title(): string
    {
        return 'flux-finances';
    }

    public function __construct($q,$month, $year,  $vehicule_id,$type)
    {
        $this->year = $year;
        $this->type = $type;
        $this->month = $month;
        $this->vehicule_id = $vehicule_id;
        $this->q = $q;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function query()
    {
        ob_end_clean(); // this
        ob_start(); // and this
        return FluxFinance::query()
                ->search($this->q)
                ->typeFluxFilter($this->type)
                ->searchPerMonthYear($this->month,$this->year)
                ->vehiculeFilter($this->vehicule_id);
    }
    public function map($flux): array
    {
        return [
                $flux->type,
                $flux->flux,
                $flux->montant,
                $flux->vehicule ? $flux->vehicule->libelle : "Inconnue",
                Carbon::parse($flux->date_transaction)->format('d/m/Y'),            
        ];
    }
    public function registerEvents(): array
    {
        $center = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]];
        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
            ],
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_NONE,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];
        return [
            AfterSheet::class    => function(AfterSheet $event) use ($styleArray,$center) {
                $cellRange = 'A1:E1'; // All headers
              $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray($styleArray);
              

            },
        ];
    }
    public function headings(): array
    {
        return [
            "Type",
            "Descriptif",
            "Montant",
            'voiture',
            'Date de transaction',
            
          
        ];
    }

    public function columnFormats(): array
    {
        return [

        ];
    }
}
