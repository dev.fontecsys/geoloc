<?php

namespace App\Http\Controllers;

use App\TypeVehicule;
use Illuminate\Http\Request;

class TypeVehiculeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TypeVehicule  $typeVehicule
     * @return \Illuminate\Http\Response
     */
    public function show(TypeVehicule $typeVehicule)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TypeVehicule  $typeVehicule
     * @return \Illuminate\Http\Response
     */
    public function edit(TypeVehicule $typeVehicule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TypeVehicule  $typeVehicule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TypeVehicule $typeVehicule)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TypeVehicule  $typeVehicule
     * @return \Illuminate\Http\Response
     */
    public function destroy(TypeVehicule $typeVehicule)
    {
        //
    }
}
