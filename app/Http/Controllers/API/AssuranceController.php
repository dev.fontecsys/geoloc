<?php

namespace App\Http\Controllers\API;

use App\Assurance;
use App\Http\Controllers\Controller;
use App\Http\Requests\AssuranceRequest;
use Illuminate\Http\Request;
use DB;
use Log;

class AssuranceController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AssuranceRequest $request)
    {

        try
        {

            //on commence la transaction
            DB::beginTransaction();

            $data = $request->toArray();

            unset($data['fichier']);

            $file = $request->input('fichier');

            $entity = Assurance::create($data);
            $entity->fresh()->load(['vehicule']);


            $entity->upload($file,"data:application/pdf;base64",$entity->vehicule->plaque_immatriculation);

            DB::commit();

            return response()->json(['success' => true,'entity'=>$entity->fresh()->unsetRelation('vehicule')->load(['assureur'])],201);
 
        }
        catch(\Exception $e)
        {
                DB::rollback();
                return ['status'=>false,'message'=>$e->getMessage()];

        }

    }

    public function getFile($id)
    {
        $assurance = Assurance::whereId($id)->first();
        $path = storage_path("/app/public/documents/assurances/".$assurance->fichier);


        if (file_exists($path)) {
            return response()->download($path);
        }
        //return response()->download($path);
    }

}
