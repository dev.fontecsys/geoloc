<?php

namespace App\Http\Requests;
 
use Illuminate\Foundation\Http\FormRequest;

class VehiculeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->isMethod('post'))
        {
            return 
            [

                //Step 0
                "libelle"=>"required|unique:vehicules",
                'nbr_porte' =>"required",
                'nbr_place' =>"required",
                'boite_vitesse' =>"required|in:Automatique,Manuelle,manuelle,automatique",
                'couleur' =>"required",
                'kilometrage' =>"required",
                'type_motorisation_id' =>"required",
                'type_vehicule_id' =>"required",
                'marque_id' =>"required",
                'modele_id' =>"required",
                'type_permis_id'=>"",
                'tarif'=>"required",
                'plaque_immatriculation'=>"required|unique:vehicules",
                'nr_chassis'=>"",
                'date_mise_en_circulation'=>"required|date",
                'consommation_au_100_km'=>"required",
    
                //step 1
    
                'volume_coffre'=>$this->input('step')=="1" ? 'nullable|numeric' : "",
                'volume_reservoir'=>$this->input('step')=="1" ? 'nullable|numeric' : "",
                'largeur'=>$this->input('step')=="1" ? 'nullable|numeric' : "",
                'longueur'=>$this->input('step')=="1" ? 'nullable|numeric' : "",
                'poids'=>$this->input('step')=="1" ? 'nullable|numeric' : "",
                'hauteur'=> $this->input('step')=="1" ? 'nullable|numeric' : "",
    
            ];
        }
        elseif($this->isMethod('patch') || $this->isMethod('put'))
        {
            return 
            [

                //Step 0
                "libelle"=>"required|unique:vehicules",
                'nbr_porte' =>"required",
                'nbr_place' =>"required",
                'boite_vitesse' =>"required|in:Automatique,Manuelle,manuelle,automatique",
                'couleur' =>"required",
                'kilometrage' =>"required",
                'type_motorisation_id' =>"required",
                'type_vehicule_id' =>"required",
                'marque_id' =>"required",
                'modele_id' =>"required",
                'type_permis_id'=>"",
                'tarif'=>"required",
                'plaque_immatriculation'=>"required|unique:vehicules",
                'nr_chassis'=>"",
                'date_mise_en_circulation'=>"required|date",
                'consommation_au_100_km'=>"required",
    
                //step 1
    
                'volume_coffre'=>'nullable|numeric',
                'volume_reservoir'=>'nullable|numeric',
                'largeur'=>'nullable|numeric',
                'longueur'=>'nullable|numeric',
                'poids'=>'nullable|numeric',
                'hauteur'=> 'nullable|numeric',
    
            ];
        }

    }


    public function messages()
    {
        return[
            //Step 0
            "libelle.required"=>"Le nom de code de la voiture est requis",
            "libelle.unique"=>"Ce nom de code est déja pris",
            'nbr_porte.required' =>"Le nombre des porte est requis",
            'nbr_place.required' =>"Le nombre de place est requis",
            'boite_vitess.required' =>"Le type de boite de vitesse est requis",
            'boite_vitess.in' =>"Ce type de boite de vitesse n'est pas autorisé",
            'couleur.required' =>"La couleur de la voiture est requise",
            'kilometrage.required' =>"Le kilomètrage de la voiture est requis",
            'type_motorisation_id.required' =>"Le type de motorisation est requis",
            'type_vehicule_id.required' =>"Le type de voiture est requis",
            'marque_id.required' =>"La marque est requise",
            'modele_id.required' =>"Le modèle est requis",
            'tarif.required'=>"Le tarif quotidien est requis",
            'plaque_immatriculation.required'=>"La plaque d'immatriculation est requise",
            'plaque_immatriculation.unique'=>"La plaque d'immatriculation est déjà prise",
            'nr_chassis.required'=>"",
            'date_mise_en_circulation.required'=>"La date de première mise en circulation est requise",
            'date_mise_en_circulation.date'=>"La date de première mise en circulation n'est pas valide",
            'consommation_au_100_km.required'=>"La consommation au 100 km est requise",

            //step 1

            'volume_coffre.required'=>"Le volume du coffre est requis",
            'volume_reservoir.required'=>"Le volume du réservoir est requis",
            'largeur.required'=>"La largeur de la voiture est requise",
            'longueur.required'=>"La longueur de la voiture est requise",
            'poids.required'=>"Le poids de la voiture est requis",
            'hauteur.required'=>"La hauteur de la voiture est requise",

            'volume_coffre.numeric'=>"Le volume du coffre doit être un nombre",
            'volume_reservoir.numeric'=>"Le volume du réservoir doit être un nombre",
            'largeur.numeric'=>"La largeur de la voiture doit être un nombre",
            'longueur.numeric'=>"La longueur de la voiture doit être un nombre",
            'poids.numeric'=>"Le poids de la voiture doit être un nombre",
            'hauteur.numeric'=>"La hauteur de la voiture doit être un nombre",

            //step 2 images


            //step 3 document
           //carte grise
           "carte_grise.date_etablissement.required"=>"La date d'établissement est requise",
           'carte_grise.date_etablissement.date' =>"La date d'établissement n'est pas valide",

           'carte_grise.proprietaire.required' =>"Le nom du propriétaire est requis",
           'carte_grise.profession.required' =>"La profession du propriétaire est requise",
           'carte_grise.adresse.required' =>"L'adresse du propriétaire est requise",
           'carte_grise.fichier.required' =>"Veuillez charger une copie de la carte grise",
           //assurance
           "assurance.nr.required"=>"Le numéro d'assurance est requis",
           'assurance.assureur.required' =>"Le nom de l'assureur est requis",
           'assurance.date_dbt.required' =>"La date de début est requise",
           'assurance.date_fin.required' =>"La date de fin est requise",
           'assurance.date_dbt.date' =>"La date de début n'est pas valide",
           'assurance.date_fin.date' =>"La date de fin n'est pas valide",
           'assurance.fichier.required' =>"Veuillez charger une copie de l'assurance",
           //visite technique
           "visite_technique.nr.required"=>"Le numéro de la visite technique est requis",
           'visite_technique.effectue_par.required' =>"Le nom de l'autorité est requis",
           'visite_technique.date_dbt.required' =>"La date de début est requise",
           'visite_technique.date_fin.required' =>"La date de fin est requise",
           'visite_technique.date_dbt.date' =>"La date de début n'est pas valide",
           'visite_technique.date_fin.date' =>"La date de fin n'est pas valide",
           'visite_technique.fichier.required' =>"Veuillez charger une copie de la visite technique",
        ];
    }
}
