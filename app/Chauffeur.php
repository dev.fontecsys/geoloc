<?php

namespace App;

use App\Traits\CanUpload;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Contracts\Activity;
use Spatie\Activitylog\Traits\LogsActivity;

class Chauffeur extends SModel
{
    use CanUpload, LogsActivity;

    protected static $logAttributes = ["nom","prenom","telephone1","telephone2","email","adresse","nr_permis_conduire","type_permis_conduire"];
    protected static $logName = 'chauffeur';
    protected static $logOnlyDirty = true;   protected static $submitEmptyLogs = false;


    protected $storage_path ="public/documents/permis";

    protected $appends = ["path","nomComplet",'hasFile'];


    
    public function tapActivity(Activity $activity, string $eventName)
    {
        $activity->description = "{$eventName}";
        if($eventName=="deleted")
        {
            $activity->as_yourself = "Vous avez supprimé le chauffeur <strong>{$this->nomComplet}</strong>";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a supprimé le chauffeur <strong>{$this->nomComplet}</strong>";
        }
        elseif($eventName=="updated")
        {
            $activity->as_yourself = "Vous avez modifié le chauffeur <strong>{$this->nomComplet}</strong>";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a modifié le chauffeur <strong>{$this->nomComplet}</strong>";
        }
        else
        {
            $activity->as_yourself = "Vous avez ajouté le chauffeur <strong>{$this->nomComplet}</strong>";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a ajouté le chauffeur <strong>{$this->nomComplet}</strong>";
        }
        
    }

    public function getPathAttribute()
    {
        return 'storage/app/public/documents/permis/'.$this->permis_conduire;
    }

    public function getHasFileAttribute()
    {
        return file_exists(storage_path("/app/public/documents/permis/".$this->permis_conduire));
    }
        /**
     * Get the chauffeur's full name.
     *
     * @return string
     */
    public function getNomCompletAttribute()
    {
        return ucfirst($this->prenom)." ".ucfirst($this->nom);
    }
    public function scopeSearch($query, $q)
    {
        if ($q == null) return $query;
        return $query
                ->orWhere('prenom', 'LIKE', "%{$q}%")
                ->orWhere('nom', 'LIKE', "%{$q}%")
                ->orWhere('email', 'LIKE', "%{$q}%")
                ->orWhere('type_permis_conduire', 'LIKE', "%{$q}%")
                ->orWhere('nr_permis_conduire', 'LIKE', "%{$q}%")
                ->orWhere('adresse', 'LIKE', "%{$q}%")
                ->orWhere('created_at', 'LIKE', "%{$q}%");
    }
}
