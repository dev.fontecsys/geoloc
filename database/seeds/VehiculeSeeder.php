<?php

use App\Marque;
use App\Modele;
use App\TypeMotorisation;
use App\TypeVehicule;
use Illuminate\Database\Seeder;

class VehiculeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $codes = [
            [
              
              
              "libelle"=> "ANCIENNE SONATA BLANCHE",
              "marque"=> "Hyundai",
              "modele"=> "SONATA",
              "immatriculation"=> "HN360AA",
              "couleur"=> "BLANCHE",
              "BV"=> "Manuelle",
              "Motorisation"=> "Essence",
              "Type_vehicule"=> "Berline"
            ],
            [
              
              
              "libelle"=> "SONATA GAEL",
              "marque"=> "Hyundai",
              "modele"=> "SONATA",
              "immatriculation"=> "HN136AA",
              "couleur"=> "Inconnue",
              "BV"=> "Manuelle",
              "Motorisation"=> "Essence",
              "Type_vehicule"=> "Berline"
            ],
            [
              
              
              "libelle"=> "ANC SONATA HJ 477 AA BLANCHE",
              "marque"=> "Hyundai",
              "modele"=> "SONATA",
              "immatriculation"=> "HJ477AA",
              "couleur"=> "BLANCHE",
              "BV"=> "Manuelle",
              "Motorisation"=> "Essence",
              "Type_vehicule"=> "Berline"
            ],
            [
              
              
              "libelle"=> "NOUVELLE SONATA GRISE GAETHAN",
              "marque"=> "Hyundai",
              "modele"=> "SONATA",
              "immatriculation"=> "HV718AA",
              "couleur"=> "GRISE",
              "BV"=> "Manuelle",
              "Motorisation"=> "Essence",
              "Type_vehicule"=> "Berline"
            ],
            [
              
              
              "libelle"=> "NOUVELLE SONATA  BLANCHE GAETHAN",
              "marque"=> "Hyundai",
              "modele"=> "SONATA",
              "immatriculation"=> "HV717AA",
              "couleur"=> "BLANCHE",
              "BV"=> "Manuelle",
              "Motorisation"=> "Essence",
              "Type_vehicule"=> "Berline"
            ],
            [
              
              
              "libelle"=> "PICANTO KIA VERTE /GRISE    135",
              "marque"=> "KIA",
              "modele"=> "PICANTO 137",
              "immatriculation"=> "HN135AA",
              "couleur"=> "VERTE /GRISE",
              "BV"=> "Manuelle",
              "Motorisation"=> "Essence",
              "Type_vehicule"=> "Berline"
            ],
            [
              
              
              "libelle"=> "PICANTO KIA BLANCHE 106",
              "marque"=> "KIA",
              "modele"=> "PICANTO  108",
              "immatriculation"=> "HN106AA",
              "couleur"=> "BLANCHE",
              "BV"=> "Manuelle",
              "Motorisation"=> "Essence",
              "Type_vehicule"=> "Berline"
            ],
            [
              
              
              "libelle"=> "PICANTO KIA MARRON 108",
              "marque"=> "KIA",
              "modele"=> "PICANTO KIA MARRON 110",
              "immatriculation"=> "HN108AA",
              "couleur"=> "MARRONNE",
              "BV"=> "Manuelle",
              "Motorisation"=> "Essence",
              "Type_vehicule"=> "Berline"
            ],
            [
              
              
              "libelle"=> "CERATO COUPE GRISE KIA 4 3",
              "marque"=> "KIA",
              "modele"=> "CERATO COUPE 4 5",
              "immatriculation"=> "HN716AA",
              "couleur"=> "GRISE",
              "BV"=> "Manuelle",
              "Motorisation"=> "Essence",
              "Type_vehicule"=> "Berline"
            ],
            [
              
              
              "libelle"=> "CERATO NOIRE",
              "marque"=> "KIA",
              "modele"=> "CERATO",
              "immatriculation"=> "XXXXXXX",
              "couleur"=> "NOIRE",
              "BV"=> "Manuelle",
              "Motorisation"=> "Essence",
              "Type_vehicule"=> "Berline"
            ],
            [
              
              
              "libelle"=> "CERATO KIA K3 NOIR",
              "marque"=> "KIA",
              "modele"=> "CERATO",
              "immatriculation"=> "HQ136AA",
              "couleur"=> "NOIRE",
              "BV"=> "Manuelle",
              "Motorisation"=> "Essence",
              "Type_vehicule"=> "Berline"
            ]
            ];
        $i=0;
        foreach($codes as $code)
        {

            $marque=Marque::where('libelle', 'LIKE', "%{$code['marque']}%")->first();
            if($marque==null)
              $marque = factory("App\Marque")->create(['libelle'=>$code['marque']]);

            $tm = TypeMotorisation::where('libelle', 'LIKE', "%{$code['Motorisation']}%")->first();
            if($tm==null)
            $tm = factory("App\TypeMotorisation")->create(['libelle'=>$code['Motorisation']]);

            $tve = TypeVehicule::where('libelle', 'LIKE', "%{$code['Type_vehicule']}%")->first();
            if($tve ==null)
            $tve  = factory("App\Marque")->create(['libelle'=>$code['Type_vehicule']]);
            
            $modele = Modele::where('libelle', 'LIKE', "%{$code['modele']}%")
                            ->where('marque_id',$marque->id)
                            ->first();

            if($modele==null)
            $modele = factory("App\Modele")->create(['libelle'=>$code['modele'],"marque_id"=>$marque->id]);

            factory('App\Vehicule')->create(['plaque_immatriculation'=>strtolower($code['immatriculation']),"couleur"=>strtolower($code['couleur']),'type_motorisation_id'=>$tm->id,'marque_id'=>$marque->id,'modele_id'=>$modele->id,'type_vehicule_id'=>$tve->id,
            "libelle"=>strtolower($code['libelle'])]);
        }

    }
}
