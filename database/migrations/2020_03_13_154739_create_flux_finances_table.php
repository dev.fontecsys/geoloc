<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFluxFinancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flux_finances', function (Blueprint $table) {
            $table->bigIncrements('id')->index();
            $table->string('flux');
            $table->double('montant');
            $table->unsignedBigInteger('vehicule_id')->nullable();
            $table->date('date_transaction');
            $table->morphs('financiable');

            $table->timestamps();
            $table->foreign('vehicule_id')->references('id')->on('vehicules')->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flux_finances');
    }
}
