<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarteGrisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carte_grises', function (Blueprint $table) {
            $table->bigIncrements('id')->index();;
            $table->date("date_etablissement");
            $table->string("profession");
            $table->string("proprietaire");
            $table->string("adresse");
            $table->date("date_premiere_mise_en_circulation")->nullable();
            $table->string("fichier",1000)->nullable();
            $table->string("nr_serie_type")->nullable();
            $table->string("nr_immatriculation");
            $table->string("nr_precedent_immatriculation")->nullable();
            $table->boolean("vehicule_neuf")->nullable();
            $table->double("poids_vide")->nullable();
            $table->double("poids_total_en_charge")->nullable();
            $table->double("charge_utile")->nullable();
            $table->integer("nbr_place")->nullable();
            $table->integer("puissance_administrative")->nullable();

            $table->string("carosserie")->nullable();
            $table->string("genre")->nullable();
            $table->unsignedBigInteger('vehicule_id');
            $table->unsignedBigInteger('marque_id')->nullable();
            $table->unsignedBigInteger('modele_id')->nullable();
            $table->unsignedBigInteger('type_motorisation_id')->nullable();

            $table->timestamps();

            $table->unique(['id','vehicule_id']);
            $table->foreign('vehicule_id')->references('id')->on('vehicules')->onDelete('cascade');
            $table->foreign('type_motorisation_id')->references('id')->on('type_motorisations')->onDelete('set null');
            $table->foreign('marque_id')->references('id')->on('marques')->onDelete('set null');
            $table->foreign('modele_id')->references('id')->on('modeles')->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carte_grises');
    }
}
