<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->define(App\VisiteTechnique::class, function (Faker $faker) {

    $date1 = Carbon::now();
    $date2 = $faker->date($format = 'Y-m-d', $max =$date1->subYears( $faker->randomElement([10,3,5,7,8,9]))->toDateString());
    $car = Carbon::parse($date2);
    $date3= $car->addYears($faker->numberBetween(1,4))->toDateString();
    $marque = factory('App\Marque')->create();
    $modele = factory('App\Modele')->create(["marque_id"=>$marque->id]);
    $vehicule = factory('App\Vehicule')->create(["marque_id"=>$marque->id,"modele_id"=>$modele->id]);

    return [

        "nr"=> Str::random(15) ,
        "date_dbt"=> $date2,
        "date_fin"=> $date2,
        "effectue_par"=> $faker->word,
        "fichier"=> Str::random(20) ,

        'vehicule_id'=> function(){ return factory('App\Vehicule')->create()->id;}



    ];
});

$factory->define(App\Assurance::class, function (Faker $faker) {

    $date1 = Carbon::now();
    $date2 = $faker->date($format = 'Y-m-d', $max =$date1->subYears( $faker->randomElement([10,3,5,7,8,9]))->toDateString());
    $car = Carbon::parse($date2);
    $date3= $car->addYears($faker->numberBetween(1,4))->toDateString();
    $marque = factory('App\Marque')->create();
    $modele = factory('App\Modele')->create(["marque_id"=>$marque->id]);
    $vehicule = factory('App\Vehicule')->create();

    return [

        "nr"=> Str::random(15) ,
        "date_dbt"=> $date2,
        "date_fin"=> $date3,
        "assureur_id"=> function(){ return factory('App\Assureur')->create()->id;},
        "fichier"=> Str::random(20) ,
        'vehicule_id'=> function(){ return factory('App\Vehicule')->create()->id;}



    ];
});

$factory->define(App\CarteGrise::class, function (Faker $faker) {

    $date1 = Carbon::now();
    $date2 = $faker->date($format = 'Y-m-d', $max =$date1->subYears( $faker->randomElement([10,3,5,7,8,9]))->toDateString());
    $poids_vide =$faker->numberBetween(1500,2000);
    $car = Carbon::parse($date2);
    $date3= $car->addYears($faker->numberBetween(1,4))->toDateString();
    $marque = factory('App\Marque')->create();
    $modele = factory('App\Modele')->create(["marque_id"=>$marque->id]);
    $vehicule = factory('App\Vehicule')->create(["marque_id"=>$marque->id,"modele_id"=>$modele->id]);

    return [

        "date_etablissement"=> $date2  ,
        "profession"=> $faker->word,
        "proprietaire"=> $faker->firstName." ".$faker->lastName,
        "adresse"=> $faker->address,
        "date_premiere_mise_en_circulation"=>$date3 ,
        "fichier"=> Str::random(20) ,
        "nr_serie_type"=> Str::random(10),
        "nr_immatriculation"=> Str::random(10),
        "nr_precedent_immatriculation"=> Str::random(10) ,
        "vehicule_neuf"=>  $faker->randomElement([true,false]),
        "poids_vide"=> $poids_vide,
        "poids_total_en_charge"=>$poids_vide + $faker->numberBetween(500,1000) ,
        "charge_utile"=>$poids_vide + $faker->numberBetween(100,450) ,
        "nbr_place"=>$faker->numberBetween(4,7) ,
        "puissance_administrative"=>$faker->numberBetween(0,10) ,

        "carosserie"=> Str::random(10),
        "genre"=>Str::random(10) ,
        'vehicule_id'=> $vehicule->id,
        'marque_id'=> $marque->id,
        'modele_id'=> $modele->id,
        'type_motorisation_id'=>$vehicule->type_motorisation_id ,



    ];
});


$factory->define(App\Image::class, function (Faker $faker) {

    return [
        'chemin' => $faker->image(storage_path('app/public/images'),640,480, null, false),
        'vehicule_id' => function(){ return factory('App\Vehicule')->create()->id;},
    ];
});
